module.exports = function generateToken() {
    return `${Math.random().toString(16).slice(2)}-${Math.random().toString(16).slice(2)}`
}