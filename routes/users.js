const router = require('express').Router()
const crypto = require('crypto')
const generateToken = require('../authorization')
const { createNewSession } = require('../sessions')

module.exports = (db) => {


    router.get('/:id', (req, res) => {
        const { id } = req.params
    
        try {
            const user = db.get('users').find({ id }).value()
            if (!user) {
                throw new Error('The user does not exist')
            }
    
            return res.status(200).json(user)
        } catch ({ message }) {
            return res.status(404).json({ error: message })
        }
    })

    router.get('/:userId/posts', (req, res) => {
        const { userId } = req.params
    
        try {
            const posts = db.get('posts').filter({ userId }).sortBy('createdAt').value().reverse()
            return res.status(200).json(posts)
        } catch ({ message }) {
            return res.status(404).json({ error: message })
        }
    })
    
    router.get('/', (req, res) => {
        const users = db.get('users').map(u => ({
            username: u.username,
            createdAt: u.createdAt,
            avatar: u.avatar,
            lastLogin: u.lastLogin || null,
            id: u.id
        }))
        return res.status(200).json(users)
    })
    
    router.post('/login', (req, res) => {
        try {
            const { username = null, password = null } = req.body
    
            if (username === null || password === null) {
                throw new Error('Provide a username and password')
            }
    
            const { SALT } = process.env
            const passwordHash = crypto.pbkdf2Sync(
                password,
                SALT,
                1000,
                64,
                'sha512'
            ).toString('hex')
    
            const user = db.get('users')
                .find({ username, password: passwordHash })
                .value()
    
            if (!user) {
                throw new Error('The credentials provided to not match')
            }
    
            const token = createNewSession(db, user.id)
    
            db.get('users')
                .find({ id: user.id })
                .assign({
                    lastLogin: Date.now()
                })
                .write()
    
            return res.status(201).json({
                id: user.id,
                username: user.username,
                createdAt: user.createdAt,
                lastLogin: user.lastLogin,
                avatar: user.avatar,
                token
            })
    
        } catch ({ message }) {
            return res.status(403).json({ error: message })
        }
    
    })
    
    router.post('/register', (req, res) => {
    
        const {
            username = null,
            password = null,
            avatar = null
        } = req.body
    
        try {
            const user = db.get('users').find({ username }).value()
            if (user) {
                throw new Error(`The username ${username} is already taken :(`)
            }
            const { SALT } = process.env
            const hash = crypto.pbkdf2Sync(
                password,
                SALT,
                1000,
                64,
                'sha512'
            ).toString('hex')
    
            const newUser = {
                id: generateToken(),
                username,
                password: hash,
                createdAt: Date.now(),
                avatar
            }
    
            db.get('users').push(newUser).write()
    
            const token = createNewSession(db, newUser.id);
    
            return res.status(201).json({
                id: newUser.id,
                createdAt: newUser.createdAt,
                username,
                avatar,
                token
            })
    
        }
        catch ({ message }) {
            return res.status(400).json({ error: message })
        }
    
    })

    return router
}