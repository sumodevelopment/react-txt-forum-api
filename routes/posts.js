const router = require('express').Router()
const generateToken = require('../authorization')

module.exports = db => {

    router.get('/', (req, res) => {
        const posts = db.get('posts').sortBy('createdAt').value().reverse()
        return res.status(200).json(posts)
    })

    router.post('/', (req, res) => {

        const {
            title,
            body,
            tags = [],
            userId,
            createdAt
        } = req.body


        try {
            if (!title  || !body || !userId) {
                throw new Error('Please provide a title, body and userId to create a new post :( ');
            }

            const { username } = db.get('users').find({ id: userId }).value()

            const newPost = {
                id: generateToken(),
                title,
                body,
                tags,
                userId,
                username,
                createdAt
            }

            db.get('posts').push(newPost).write()

            return res.status(201).json(newPost)

        }
        catch ({ message }) {
            return res.status(404).json({ error: message })
        }


    })

    router.patch('/:id', (req, res) => {
        const { id } = req.params
        const { updatedAt } = req.body

        try {
            const post = db.get('posts').find({ id: id }).value()
            if (!post) {
                throw new Error('That post does not exist')
            }

            const {
                title = null,
                body = null,
                tags = null
            } = req.body

            if (title !== null) {
                post.title = title
            }

            if (body !== null) {
                post.body = body
            }

            if (tags !== null) {
                post.tags = tags
            }

            db.get('posts').assign({ ...post, updatedAt }).write()

            return res.status(200).json({ ...post, updatedAt })

        } catch ({ message }) {
            return res.status(404).json({ error: message })
        }

    })

    return router
}