const router = require('express').Router()

module.exports = db => {

    router.get('/', (req, res) => {
        const avatars = db.get('avatars')
        return res.status(200).json(avatars)
    })

    router.get('/:id', (req, res) => {
        const { id } = req.params
        try {
            const avatar = db.get('avatars').find({ id: +id }).value()
            if (!avatar) {
                throw new Error('Avatar not found')
            }
            return res.status(200).json(avatar)
        } catch ({ message }) {
            return res.status(404).json({ error: message })
        }
    })

    return router
}