# React Txt Forum Server

Server application in Node for React TXT Forum.

> 🏆 Built with JSON-Server [https://github.com/typicode/json-server](https://github.com/typicode/json-server)

# Available Endpoints

The base URL of the API is at: https://noroff-react-txt-forum-api.herokuapp.com/

Posts that are marked with the lock icon (🔒) require a valid authentication token.

## `/avatars`
Each user registered may have an avatar. The Avatars used the [DiceBear](https://avatars.dicebear.com/) image generator.
### `GET /avatars`
Get a list of the available avatars

#### Example Response
```json
[
  {
    "id": 1,
    "url": "https://noroff-react-txt-forum-api.herokuapp.com/assets/images/001.svg"
  },
  ...
]
```

### 🔒 `GET /avatars/:id`
Get an avatar with a specific id. requires and Authentication token.

#### Example Response
```json
[
  {
    "id": 1,
    "url": "https://noroff-react-txt-forum-api.herokuapp.com/assets/images/001.svg"
  }
]
```

## `/users`
The users endpoint is used mainly to login and register users.
### `GET /users`

Retrieve a list of users.

### 🔒 `GET /users/:id`

Retrieve a user with a specific id.
### 🔒 `GET /users/:userId/posts`

Retrieve all the posts for a specific user matching the given user id.
### `POST /users/login`

Login and authenticate a user based on their username and password combination.
### `POST /users/register`

Create a new user.

## `/posts`

The posts endpoint allows for managing posts. The posts endpoint is protected with a Basic Access Token. The is returned from the Login/Register endpoints. 

### 🔒 `GET /posts`

Retrieve all the posts.

### 🔒 `POST /posts`

Create a new post.

### 🔒 `PATCH /posts/:id`

Update a specific post matching the given post id.

# 🏆 Special Thanks

- [Typicode/JsonServer](https://github.com/typicode/json-server)
- [DiceBear Avatar generator](https://avatars.dicebear.com)

# Contributions

- Dewald Els - Author