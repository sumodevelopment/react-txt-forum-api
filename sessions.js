const generateToken = require('./authorization')

function createNewSession(db, userId) {
    const token = generateToken()

    const userSession = db.get('sessions').find({ userId }).value()

    if (!userSession) {
        db.get('sessions').push({
            token,
            userId,
            createdAt: Date.now()
        }).write()
    }
    else {
        db.get('sessions')
            .find({
                userId
            })
            .assign({ token })
            .write()
    }


    return token
}

module.exports = {
    createNewSession
}