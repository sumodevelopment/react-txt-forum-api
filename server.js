const express = require('express')
const app = express()
const { PORT = 5000 } = process.env
require('dotenv').config()

const lowdb = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = lowdb(adapter)
const cors = require('cors')

db.defaults({
    users: [],
    posts: [],
    sessions: []
}).write()

app.use( cors() )
app.use(express.json())

app.use(express.static(__dirname + '/public'))

const authMiddleware = require('./middlewares/authorization-token')(db)
app.use(authMiddleware)
const dateMiddleware = require('./middlewares/dates')
app.use(dateMiddleware)

const userRoutes = require('./routes/users')(db)
const postsRoutes = require('./routes/posts')(db)
const avatarRoutes = require('./routes/avatars')(db)
const authRoutes = require('./routes/auth')(db)

app.use('/users', userRoutes)
app.use('/posts', postsRoutes)
app.use('/avatars', avatarRoutes)
app.use('/auth', authRoutes)


app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}...`);
})