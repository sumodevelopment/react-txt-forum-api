module.exports = db => (req, res, next) => {

    const PUBLIC_PATHS = ['/users/login', '/users/register', '/avatars', '/auth/verify']

    const { url } = req

    if (PUBLIC_PATHS.includes(url) || url.indexOf('assets/images') > -1) {
        return next()
    }

    const { authorization = null } = req.headers

    try {

        if (authorization === null) {
            throw new Error('You are not authorized to access this resource. Please provide a valid token')
        }

        const [type, token = null] = authorization.split(' ')

        if (type !== 'Bearer' || !token === null) {
            throw new Error('Invalid authorization header or no token received')
        }

        const session = db.get('sessions').find({ token }).value()

        if (!session) {
            throw new Error('Invalid authorization token received.')
        }

        req.body.token = token;

        next()

    } catch ({ message }) {
        return res.status(401).json({ error: message })
    }

}